from typing import Optional


class CaesarCipher:

    def __init__(self, key: int) -> None:
        self.key = key

    def encrypt(self, text: Optional[str]) -> str:
        if not text:
            raise ValueError("Texto não pode ser nulo")

        return ''.join(
            chr(ord(c) + self.key) if c.isalpha() else c
            for c in text.lower()
        )

    def decrypt(self, text: Optional[str]) -> str:
        if not text:
            raise ValueError("Texto não pode ser nulo")

        return ''.join(
            chr(ord(c) - self.key) if c.isalpha() else c
            for c in text.lower()
        )

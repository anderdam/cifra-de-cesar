Código original -> [Cifra de Cesar (Java) / Github](https://github.com/anderdam/Cifra-de-Cesar-Code-and-Decode-).
<p>Foi um desafio em Java que fiz para um treinamento há alguns anos, só que convertendo para Python e 
ajustando o gitlab-ci.yml mostrado em aula para realizar o build.

<p>Prints:</p>

<span><b> Print 1 - builds </span>

![Print 1 - builds](https://imagizer.imageshack.com/img923/3071/4MWFid.jpg)

<span><b> Print 2 - Saída Esperada: </span>

![Print 2 - Saída Esperada](https://imagizer.imageshack.com/img924/203/mzBQg2.jpg)


import unittest

from CaesarCipher import CaesarCipher


class TestCriptografia(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.criptografia = CaesarCipher(3)

    def test_deve_retornar_erro_quando_o_texto_a_ser_criptografado_eh_vazio(self):
        with self.assertRaises(expected_exception=ValueError):
            self.criptografia.encrypt("")

    def test_deve_retornar_erro_quando_o_texto_a_ser_criptografado_eh_nulo(self):
        with self.assertRaises(expected_exception=ValueError):
            self.criptografia.encrypt(None)

    def test_deve_retornar_erro_quando_o_texto_a_ser_descriptografado_eh_vazio(self):
        with self.assertRaises(ValueError):
            self.criptografia.decrypt("")

    def test_deve_retornar_erro_quando_o_texto_a_ser_descriptografado_eh_nulo(self):
        with self.assertRaises(ValueError):
            self.criptografia.decrypt(None)

    def test_deve_criptografar_o_texto(self):
        texto = "a ligeira raposa marrom saltou sobre o cachorro cansado"
        texto_criptografado = self.criptografia.encrypt(texto)
        self.assertEqual(texto_criptografado, "d oljhlud udsrvd pduurp vdowrx vreuh r fdfkruur fdqvdgr")

    def test_deve_descriptografar_o_texto(self):
        texto_criptografado = "d oljhlud udsrvd pduurp vdowrx vreuh r fdfkruur fdqvdgr"
        texto_descriptografado = self.criptografia.decrypt(texto_criptografado)
        self.assertEqual(texto_descriptografado, "a ligeira raposa marrom saltou sobre o cachorro cansado")

    def test_deve_manter_os_numeros_na_criptografia(self):
        texto = "sejam bem vindos ao Acelera Brasil 2019"
        texto_criptografado = self.criptografia.encrypt(texto)
        self.assertEqual(texto_criptografado, "vhmdp ehp ylqgrv dr dfhohud eudvlo 2019")

    def test_deve_manter_os_numeros_na_descriptografia(self):
        texto_criptografado = "vhmdp ehp ylqgrv dr dfhohud eudvlo 2019"
        texto_descriptografado = self.criptografia.decrypt(texto_criptografado)
        self.assertEqual(texto_descriptografado, "sejam bem vindos ao acelera brasil 2019")

    def test_deve_converter_caracteres_para_minusculo_na_criptografia(self):
        texto = "Aprender Java gera felicidade"
        texto_criptografado = self.criptografia.encrypt(texto)
        self.assertEqual(texto_criptografado, "dsuhqghu mdyd jhud iholflgdgh")

    def test_deve_converter_caracteres_para_minusculo_na_descriptografia(self):
        texto_criptografado = "Dsuhqghu Mdyd jhud iholflgdgh"
        texto_descriptografado = self.criptografia.decrypt(texto_criptografado)
        self.assertEqual(texto_descriptografado, "aprender java gera felicidade")


if __name__ == "__main__":
    unittest.main()
